import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { User } from './users';
import { Post } from './posts';
import { Comment } from './comments';

@Injectable({
  providedIn: 'root'
})
export class ApiDataService {

  usersUrl = 'https://jsonplaceholder.typicode.com/users';

  constructor(private http: HttpClient) { }

  getUsers()  {
    return this.http.get<User[]>(this.usersUrl);
  }
}
