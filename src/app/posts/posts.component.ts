import { Component, OnInit } from '@angular/core';
import { User } from '../users';
import { Post } from '../posts';
import { Comment } from '../comments';
import { ApiDataService } from '../api-data.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  users: User[];
  posts: Post[];
  comments: Comment[];
  UserId: number;
  postId: number;

  postUrl: string;
  commentUrl: string;

  constructor(private dataService: ApiDataService, private http: HttpClient) { }

  ngOnInit() {
    return this.dataService.getUsers()
      .subscribe(data => this.users = data);

  }

  onUserSelected(val:any) {
    this.userPostFunction(val);

  }

  userPostFunction(val:any) {
    this.postUrl = "https://jsonplaceholder.typicode.com/posts?userId=" + val;
    return this.http.get<Post[]>(this.postUrl)
      .subscribe(data => this.posts = data);
  }

  userCommentFunction(val:any) {
    this.postUrl = "https://jsonplaceholder.typicode.com/comments?userId=" + val;
    return this.http.get<Comment[]>(this.commentUrl)
      .subscribe(data => this.comments = data);

  }


}
